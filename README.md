<div align="center">
<h1>CAST</h1>
</div>

<p align="center">
<img alt="" src="https://img.shields.io/badge/release-v0.1.1-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjc-v0.55.3-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjcov-90.0%25-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/state-孵化-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/domain-HOS/Cloud-brightgreen" style="display: inline-block;" />
</p>


# cast

一个方便安全的类型转化库

## 引入 cast

在你项目中的`cjpm.toml`文件中引入以下配置后，运行`cjpm update`

```
[dependencies]
cast = {git = "https://gitcode.com/abandon1a2b/cast.git", branch = "main"}
```

## 使用

```cj

import cast;

main(): Unit {
    println(cast.toBool(1))
    println(cast.toBool(1.1))
    println(cast.toBool("true"))
    println(cast.toBool("false"))
    println(cast.toInt16(1))
    println(cast.toInt16(1.1))
    println(cast.toInt16("true"))
    println(cast.toInt16("false"))
    println(cast.toFloat64(1))
    println(cast.toFloat64(1.1))
    println(cast.toFloat64("true"))
    println(cast.toFloat64("false"))
    return 
}
```

## 函数列表

```cj
cast.toInt8(value)        
cast.toInt16(value)        
cast.toInt32(value)        
cast.toInt64(value)        
cast.toIntNative(value)        
cast.toUInt8(value)        
cast.toUInt16(value)        
cast.toUInt32(value)        
cast.toUInt64(value)        
cast.toUIntNative(value)        
cast.toFloat16(value)        
cast.toFloat32(value)        
cast.toFloat64(value)        
cast.toBool(value)        
cast.toRune(value)        
cast.toString(value)
```

## 1 介绍

### 1.1 项目特性

1. 提供简易切安全的类型转化

2. 支持现版本所有的基础字段转化


### 1.2 项目计划

1. 2024 年 11 月发布 0.1.0 版本。
